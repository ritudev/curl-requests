import os
import sys
import io

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

here = os.path.abspath(os.path.dirname(__file__))

about = {}
with io.open(os.path.join(here, 'curl_requests', '__version__.py'), 'r', encoding='utf-8') as f:
    exec(f.read(), about)

with io.open('README.md', 'r', encoding='utf-8') as f:
    readme = f.read()
# with io.open('HISTORY.md', 'r', encoding='utf-8') as f:
#     history = f.read()

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

packages = [
    'curl_requests',
]

requires = [
    'requests>=1.2.0',
    'pycurl>=7.43.0',
    'certifi>=2017.4.17',
    'six'
]

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=packages,
    package_dir={'curl_requests': 'curl_requests'},
    package_data={'curl_requests': ['LICENSE', 'README.md']},
    include_package_data=True,
    install_requires=requires,
    setup_requires=['setuptools>=38.6.1'],
    license=about['__license__'],
    zip_safe=False,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
