from distutils.version import LooseVersion
import six
import time
import threading
import uuid
import re
from collections import OrderedDict
import pycurl
import certifi
# from utils import _AttributeDict, infinite_defaultdict
from http.cookies import SimpleCookie
import requests
from requests.structures import CaseInsensitiveDict
from requests.sessions import merge_setting
from requests import models

from requests.compat import is_py2, is_py3, urljoin, urlencode, unquote, basestring, cookielib
from requests.utils import requote_uri

from .exceptions import translate_curl_exception
from . import DEBUG, VERBOSE

try:
    from requests_toolbelt.multipart.encoder import MultipartEncoder
except ImportError:
    MultipartEncoder = None

try:
    import simplejson as complexjson
except ImportError:
    import json as complexjson

try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

try:
    import Queue as queue
except ImportError:
    import queue

VERSION = re.search(r'PycURL/(.+?)\s', pycurl.version, flags=re.I).group(1)  # '7.43.0.3'
IS_MODERN_PYCURL = LooseVersion(VERSION) >= LooseVersion('7.43.0.3')
BR_SUPPORT = True if 'brotli' in pycurl.version else False
BR_REGEX = re.compile(r",?\s*?\bbr\b")

HTTP_GENERAL_RESPONSE_HEADER = re.compile(r"(?P<version>HTTP\/.*?)\s+(?P<code>\d{3})\s+(?P<message>.*)")

# https://github.com/hhstore/annotated-py-tornado/blob/master/src/tornado-1.0.0/tornado/httpclient.py#L682
# Set the request method through curl's retarded interface which makes
# up names for almost every single method
CURL_OPTIONS = {
    "GET": pycurl.HTTPGET,
    "POST": pycurl.POST,
    "PUT": pycurl.UPLOAD,
    "HEAD": pycurl.NOBODY,
}
# logger = getLogger("curl_requests")


def make_str(value, encoding='utf-8', errors='strict'):
    """
    Normalize unicode/byte string to byte string.
    """

    if isinstance(value, six.text_type):
        return value.encode(encoding, errors=errors)
    elif isinstance(value, six.binary_type):
        return value
    else:
        return six.u(str(value)).encode(encoding, errors=errors)


def get_newest_dict_item(d):
    newest_key = next(reversed(d))  # under lock to prevent StopIteration on next()
    return (newest_key, d[newest_key])


def remove_handle(curl_session, curl, request, response):
    with curl_session.lock:
        curl_session.m.remove_handle(curl)
        # del curl_session.curl_dict[request.id]  # moved to result()
        curl_session.req_count -= 1
    curl_session.free_handle_queue.put(curl)


class CurlRequest(object):
    # __repr__ = models.Request.__dict__['__repr__']

    def __init__(self, method='GET', url=None, headers=None, data=None, params=None, json=None):
        self.url = url
        self.method = method
        self.headers = headers
        self.data = data
        self.params = params
        self.json = json

        self.pending = None
        self._processed_result = False

    def __repr__(self):
        assert self.pending is not None
        if self.pending:
            return '<RunningRequest [%s]>' % (self.method)
        else:
            return '<Request [%s]>' % (self.method)

    def result(self):
        # curl = self.curl
        curl = self.curl_session.curl_dict[self.id]['curl']
        request = self.curl_session.curl_dict[self.id]['request']
        response = self.curl_session.curl_dict[self.id]['response']
        assert request is self

        while 1:
            try:
                if DEBUG:
                    print("self.curl_session.curl_dict[self.id]['queue'].get(timeout=self.timeout+3)", self.id)
                request_result = self.curl_session.curl_dict[self.id]['queue'].get(timeout=self.timeout + 3)
                if DEBUG:
                    print("got self.curl_session.curl_dict[self.id]['queue'].get(timeout=self.timeout+3)", self.id)
            except queue.Empty:
                raise translate_curl_exception(
                    pycurl.E_OPERATION_TIMEDOUT,
                    '[No result, should not happen] Operation timed out after {} milliseconds'.format(
                        (self.timeout + 3) * 1000
                    )
                )

            if request_result == 'perform':
                self.curl_session._perform(performer_curl=curl)
                continue
            else:
                status, errno, errmsg = request_result
                r = self.process_result(status, curl, self, response, errno, errmsg)
                with curl.curl_dict_lock:
                    del self.curl_session.curl_dict[request.id]
                # del curl.r
                # del curl.request
                # self.curl_session.free_handle_queue.put(curl)
                if DEBUG:
                    print('self.curl_session.free_handle_queue.qsize()', self.curl_session.free_handle_queue.qsize())
                return r

    def process_result(self, status, curl, request, response, errno, errmsg):
        self.pending = False
        if status == 'success':
            if self._processed_result:
                # assert would fail here because the curl's could differ when non-blocking and not calling .result() before all the curl handles get exhausted
                return response
            else:
                assert self is request
                assert request is curl.request

            self._processed_result = True

            body = response._body_output.getvalue()
            response._body_output.truncate(0)
            response._body_output.seek(0)
            response._body_output.getvalue()
            response._body_output.close()  # not needed because it gets garbage collected even if still open
            if DEBUG:
                print('self.curl_session.thread', self.curl_session.thread)
            r = self.curl_session.curl_to_requests(curl, url=request.url, body=body)

            if DEBUG:
                print("Success:", r.status_code, r.request.url, r.url, "content:", r.content[:10], "text:", r.text[:10])

            remove_handle(self.curl_session, curl, request, response)
            return r
        elif status == 'error':
            if self._processed_result:
                # "assert self is request" would fail here because the curl's could differ when non-blocking and not calling .result() before all the curl handles get exhausted
                assert threading.current_thread().name != 'MultiPerformer'
                raise translate_curl_exception(errno, errmsg)
                # return
            else:
                assert self is request
                assert request is curl.request

            self._processed_result = True

            # errors:
            #   no internet: ('Failed: ', 'https://httpbin.org/ip', 28, 'Resolving timed out after 60339 milliseconds')
            if DEBUG:
                #   ('Failed: ', 'https://httpbin.org/redirect/3', 47, 'Maximum (0) redirects followed')
                # print("Failed:", r.status_code, r.request.url, errno, errmsg, "content:", r.content[:10], "text:", r.text[:10])
                print("Failed:", errno, errmsg)

            response._headers_output.truncate(0)
            response._headers_output.seek(0)
            response._headers_output.close()
            response._headers_output = None

            response._body_output.truncate(0)
            response._body_output.seek(0)
            response._body_output.close()
            response._body_output = None
            remove_handle(self.curl_session, curl, request, response)
            if threading.current_thread().name != 'MultiPerformer':
                raise translate_curl_exception(errno, errmsg)


class Elapsed(object):
    def __init__(self, total_time):
        #dt1 =
        #dt2 =
        self.total_time = total_time

    def total_seconds(self):
        return self.total_time


class CurlGetInfo(object):
    def __init__(self):
        self.version = None
        self.total_time = None


class CurlResponse(object):
    # https://github.com/psf/requests/blob/bedd9284c9646e50c10b3defdf519d4ba479e2c7/requests/models.py#L586
    __attrs__ = ['_content', 'status_code', 'headers', 'url', 'history', 'reason', 'cookies', 'elapsed', 'request']

    is_redirect = models.Response.__dict__['is_redirect']
    apparent_encoding = models.Response.__dict__['apparent_encoding']
    text = models.Response.__dict__['text']
    __repr__ = models.Response.__dict__['__repr__']
    __bool__ = models.Response.__dict__['__bool__']

    # __nonzero__ = models.Response.__dict__['__nonzero__']  # TODO: add raise_for_status()

    def __init__(self, request=None):
        # Request:
        self.request = request

        # Response:
        # BytesIO object for response headers
        self._headers_output = None
        # BytesIO object for response body
        self._body_output = None

        # Response:
        self._parsed_headers = False
        self._headers = None
        # self.headers = None
        # self._content = b''
        # self.content = b''
        self._body = b''
        self._text = None

        # :Response status code
        self._status_code = None

        #: Final URL location of Response.
        self.url = None

        #: Encoding to decode with when accessing r.text.
        self.encoding = None

        # Redirects history
        self._history = []

        # list of parsed headers blocks
        self._headers_history = []

        self.getinfo = CurlGetInfo()

    @property
    def content(self):
        return self._body

    @property
    def _content(self):
        return self._body

    @_content.setter
    def _content(self, value):
        self._body = value

    def json(self):
        return complexjson.loads(self.content)

    @property
    def headers(self):
        """Returns response headers
        """
        if not self._headers:
            self._parse_headers_raw()
        return self._headers

    # @property
    # def text(self):
    #   if self._text is not None:
    #       return self._text
    #   else:
    #       # Figure out what encoding was sent with the response, if any.
    #       # Check against lowercased header name.
    #       encoding = None
    #       if 'content-type' in self.headers:
    #           content_type = self.headers['content-type'].lower()
    #           match = re.search('charset=(\S+)', content_type)
    #           if match:
    #               encoding = match.group(1)
    #               print('Decoding using %s' % encoding)
    #       if encoding is None:
    #           # Default encoding for HTML is iso-8859-1.
    #           # Other content types may have different default encoding,
    #           # or in case of binary data, may have no encoding at all.
    #           encoding = 'iso-8859-1'
    #           print('Assuming encoding is %s' % encoding)

    #       # Decode using the encoding we figured out.
    #       self._text = self._content.decode(encoding)
    #       return self._text

    # @property
    # def status_code(self):
    #   if not self._status_code:
    #       self._status_code = int(self.curl.getinfo(pycurl.HTTP_CODE))
    #   return self._status_code

    @staticmethod
    def _split_headers_blocks(raw_headers):
        # HTTP standard specifies that headers are encoded in iso-8859-1.
        # On Python 2, decoding step can be skipped.
        # On Python 3, decoding step is required.
        raw_headers = raw_headers.decode('iso-8859-1')

        i = 0
        blocks = []
        for item in raw_headers.strip().split("\r\n"):
            if item.startswith("HTTP"):
                blocks.append([item])
                i = len(blocks) - 1
            elif item:
                blocks[i].append(item)
        return blocks

    def _parse_headers_raw(self):
        """Parse response headers and save as instance vars
        """
        def parse_header_block(raw_block):
            """Parse headers block
            Arguments:
            - `block`: raw header block
            Returns:
            - `headers_list`:
            """
            block_headers = []
            for header in raw_block:
                if not header:
                    continue

                if not header.startswith("HTTP"):
                    # Header lines include the first status line (HTTP/1.x ...).
                    # We are going to ignore all lines that don't have a colon in them.
                    # This will botch headers that are split on multiple lines...
                    if ':' not in header:
                        continue

                    # Break the header line into header name and value.
                    # name, value = map(lambda u: u.strip(), header.split(":", 1))
                    name, value = header.split(':', 1)

                    # Remove whitespace that may be present.
                    # Header lines include the trailing newline, and there may be whitespace
                    # around the colon.
                    name = name.strip()
                    value = value.strip()

                    if name.startswith("Location"):
                        # add the first url (non the location url) as the first redirect
                        if not self._history and self.request.allow_redirects:
                            self._history.append(self.request.url)

                        # maybe not good
                        if not value.startswith("http"):
                            value = urljoin(self.url, value)
                        self._history.append(value)
                    if value[:1] == value[-1:] == '"':
                        value = value[1:-1]  # strip "
                    block_headers.append((name, value.strip()))
                else:
                    # extract version, code, message from first header
                    try:
                        version, code, message = HTTP_GENERAL_RESPONSE_HEADER.findall(header)[0]
                    except Exception as e:
                        logger.warn(e)
                        continue
                    else:
                        block_headers.append((version, code, message))
            return block_headers

        raw_headers = self._headers_output.getvalue()
        self._headers_output.truncate(0)
        self._headers_output.seek(0)
        self._headers_output.close()
        # self._headers_output.close()  # not needed because it gets garbage collected even if still open | don't close in case that .headers get called twice simultaneously.

        for raw_block in self._split_headers_blocks(raw_headers):
            block = parse_header_block(raw_block)
            self._headers_history.append(block)

        last_header = self._headers_history[
            -1]  # [('HTTP/1.1', '200', 'OK'), ('Content-Type', 'text/javascript; charset=utf-8')...
        if not self.getinfo.version:
            self.getinfo.version = last_header[0][0]  # Possible versions: "HTTP/1.0", HTTP/1.1", "HTTP/2.0"
        self._headers = CaseInsensitiveDict(last_header[1:])

        # commented out to keep the history empty when there are no redirects
        # if not self._history:
        #   print('_history_history_history_history', self._history)
        #   self._history.append(self.url)

        # delete the destination url from the history
        if self._history:
            del self._history[-1]

        self._parsed_headers = True

    @property
    def history(self):
        """Returns redirects history list
        :return: list of redirect URL's (TODO: list of `Response` objects)
        """
        if not self._history and not self._parsed_headers:
            self._parse_headers_raw()
        return self._history

    def close(self):
        return

    @property
    def ok(self):
        """Returns True if :attr:`status_code` is less than 400, False if not.

        This attribute checks if the status code of the response is between
        400 and 600 to see if there was a client error or a server error. If
        the status code is between 200 and 400, this will return True. This
        is **not** a check to see if the response code is ``200 OK``.
        """
        if 400 <= self.status_code < 500:
            return False  # Client Error
        elif 500 <= self.status_code < 600:
            return False  # Server Error
        else:
            return True

    # def __del__(self):
    #     """ Help prevent memory leaks.
    #     """
    #     print ('del', self)
    #     try:
    #         if self._headers_output is not None:
    #             self._headers_output.close()
    #             del self._headers_output
    #             self._headers_output = None
    #     except AttributeError:
    #         pass
    #     try:
    #         if self._body_output is not None:
    #             self._body_output.close()
    #             del self._body_output
    #             self._body_output = None
    #     except AttributeError:
    #         pass
    #     try:
    #         print ('del body', self)
    #         del self._body
    #         print ('del body done', self)
    #     except AttributeError:
    #         pass
    #     try:
    #         del self._text
    #     except AttributeError:
    #         pass
    #     try:
    #         del self._headers
    #     except AttributeError:
    #         pass


def get_proxy(proxies):
    # requests proxy format is a dict, curl proxy is a string without the protocol.
    if proxies:
        try:
            proxy = proxies['http']
        except KeyError:
            proxy = proxies['https']
        except TypeError:
            # TypeError: string indices must be integers, not str
            proxy = proxies  # already a string

        proxy = proxy.replace('http://', '').replace('https://', '').replace('socks4://', '').replace('socks5://', '')
        return proxy
    else:
        return None


def str_to_bool(value):
    if value == 'TRUE':
        return True
    elif value == 'FALSE':
        return False
    else:
        return value


multi_info_task_queue_results = {}


def parse_cookie(curl, curl_session):
    # http://www.cookiecentral.com/faq/

    # https://curl.haxx.se/libcurl/c/CURLOPT_COOKIELIST.html
    # char *my_cookie =
    #   "example.com"    /* Hostname */
    #   SEP "FALSE"      /* Include subdomains */
    #   SEP "/"          /* Path */
    #   SEP "FALSE"      /* Secure */
    #   SEP "0"          /* Expiry in epoch time format. 0 == Session */
    #   SEP "foo"        /* Name */
    #   SEP "bar";       /* Value */

    schema = 'requests'  # requests / browser

    if not IS_MODERN_PYCURL and requests.compat.is_py3:
        # https://github.com/dimichxp/pycurl/commit/4df7a0e5bb38a3db5f04721add571cd32c5e3eb8
        # pycurl bug in python3 only:
        # curl.getinfo(pycurl.INFO_COOKIELIST): [<NULL>, <NULL>, <NULL>, <NULL>, <NULL>]
        # Segmentation fault (core dumped)
        # fixed in pycurl 7.43.0.3
        cookies_curl = []
        print(
            "WARNING: pycurl.INFO_COOKIELIST not supported, can't display the cookies with curl_session.cookies but they will still work in curl."
        )
    elif curl_session.use_curlmulti_thread:
        if threading.current_thread().name == 'MultiPerformer':
            # if process_result()->curl_to_requests()->parse_cookie() is called from _perform by the MultiPerformer thread,
            # then instead of passing the getinfo task to _perform() (which it won't be able to execute because the thread is >here< right now),
            # just get the cookies normally.
            with curl_session.lock:
                cookies_curl = curl.getinfo(pycurl.INFO_COOKIELIST)
        else:
            # return []#temp
            # even when there's a lock it sometimes throws "cannot invoke getinfo() - perform() is currently running"
            result_id = str(uuid.uuid4())
            multi_info_task_queue_results[result_id] = {'queue': queue.Queue()}
            curl_session.multi_queue.put((result_id, curl, 'getinfo', pycurl.INFO_COOKIELIST))
            if DEBUG:
                print('curl_session.thread', curl_session.thread, 'cookies on', threading.current_thread().name)
            cookies_curl = multi_info_task_queue_results[result_id]['queue'].get(timeout=10)
            del multi_info_task_queue_results[result_id]
    else:
        with curl_session.lock:
            cookies_curl = curl.getinfo(pycurl.INFO_COOKIELIST)

    if DEBUG:
        print('cookies_curl (COOKIELIST):', 'type', type(cookies_curl), 'cookies_curl', cookies_curl)

    cookies = []
    for item in cookies_curl:
        domain, flag, path, secure, expiry, \
        name, value = item.split("\t")
        if DEBUG:
            print('domain, flag, path, secure, expiry, name, value', domain, flag, path, secure, expiry, \
                name, value)
        if domain.startswith('#HttpOnly_'):
            http_only = True
            domain = domain[len('#HttpOnly_'):]
        else:
            http_only = False
        if schema == 'requests':
            cookie = Cookie(
                domain=domain,
                flag=str_to_bool(flag),
                path=path,
                expires=None if expiry == '0' else expiry,
                name=name,
                value=value,
                rest={'HttpOnly': http_only if http_only else None},
                secure=str_to_bool(secure)
            )
            cookies.append(cookie)
        # elif schema == 'browser':
        #   cookies.append(_AttributeDict({
        #       'domain': domain.lower(),
        #       'domainSpecified': str_to_bool(flag),
        #       'path': path,
        #       'pathSpecified': str_to_bool(secure),
        #       'expiry': int(expiry),
        #       'name': name,
        #       'value': value,
        #       'httpOnly': http_only,
        #       'secure': secure
        #   }))
        else:
            raise ValueError('unknown cookie schema: {}'.format(schema))

    for cookie in cookies:
        curl_session.cookies.renew_previously_removed(cookie)

    if DEBUG:
        print('result cookies', cookies)
    return cookies


# https://github.com/python/cpython/blob/master/Lib/http/cookiejar.py#L730
class Cookie(object):
    # https://github.com/python/cpython/blob/master/Lib/http/cookiejar.py#L797
    is_expired = cookielib.Cookie.__dict__['is_expired']

    def __init__(self, name=None, value=None, domain=None, flag=None, path=None, expires=None, rest=None, secure=False):
        self.name = name
        self.value = value
        if domain:
            domain = domain.lower()
        self.domain = domain

        if domain is not None:
            if domain[0] == '.':
                self.domain_no_dot = domain[1:]
                flag = True
            elif domain.startswith('#HttpOnly_.'):
                self.domain_no_dot = domain.replace('#HttpOnly_.', '', 1)
                flag = True
            else:
                self.domain_no_dot = domain
        else:
            self.domain_no_dot = domain

        self.flag = flag
        if self.domain_no_dot != self.domain:
            # if domain starts with a dot then flag must be true
            assert self.flag is True

        self.path = path
        self.secure = secure

        if expires is not None:
            assert expires != 0
            assert expires != '0'
            expires = int(float(expires))
        self.expires = expires

        if rest is None:
            rest = {'HttpOnly': None}
        self._rest = rest

        # not used
        self.port = None  # TODO

        # no longer used
        # self._cookie_dict = _AttributeDict(name=self.name, value=self.value, domain=self.domain, path=self.path, expires=self.expires, rest=self._rest, secure=self.secure)

    def __str__(self):
        if self.port is None: p = ""
        else: p = ":" + self.port
        limit = self.domain + p + self.path
        if self.value is not None:
            namevalue = "%s=%s" % (self.name, self.value)
        else:
            namevalue = self.name
        return "<Cookie %s for %s>" % (namevalue, limit)

    def __repr__(self):
        args = []
        # for name in ("version", "name", "value",
        #            "port", "port_specified",
        #            "domain", "domain_specified", "domain_initial_dot",
        #            "path", "path_specified",
        #            "secure", "expires", "discard", "comment", "comment_url",
        #            ):
        for name in (
            "name",
            "value",
            "domain",
            "path",
            "secure",
            "expires",
        ):
            attr = getattr(self, name)
            args.append("%s=%s" % (name, repr(attr)))
        args.append("rest=%s" % repr(self._rest))
        # args.append("rfc2109=%s" % repr(self.rfc2109))
        return "%s(%s)" % (self.__class__.__name__, ", ".join(args))


class Cookies(object):
    # https://github.com/kennethreitz/requests/blob/75bdc998e2d430a35d869b2abf1779bd0d34890e/requests/cookies.py
    get = requests.cookies.RequestsCookieJar.__dict__['get']
    # _find_no_duplicates = requests.cookies.RequestsCookieJar.__dict__['_find_no_duplicates']  # for .pop()
    __delitem__ = requests.cookies.RequestsCookieJar.__dict__['__delitem__']

    def __init__(self, curl_session):
        self.curl_session = curl_session
        self.lock = threading.Lock()
        self._cookies = []
        # self._removed_cookies_dict = infinite_defaultdict()  # d[domain][path][name] = cookie
        self._removed_cookies_dict = {}  # d[domain][path][name] = cookie

    def remove_cookie_by_name(self, name, domain=None, path=None):
        """Unsets a cookie by name, by default over all domains and paths.
        Wraps CookieJar.clear(), is O(n).
        """
        clearables = []
        for cookie in self._cookies:
            if cookie.name != name:
                continue
            if domain is not None and domain != cookie.domain:
                continue
            if path is not None and path != cookie.path:
                continue
            clearables.append((cookie.domain, cookie.path, cookie.name))

        for domain, path, name in clearables:
            self.clear(domain, path, name)

    # https://github.com/kennethreitz/requests/blob/75bdc998e2d430a35d869b2abf1779bd0d34890e/requests/cookies.py#L299
    def get_dict(self, domain=None, path=None):
        """Takes as an argument an optional domain and path and returns a plain
        old Python dict of name-value pairs of cookies that meet the
        requirements.
        :rtype: dict
        """
        dictionary = {}
        # for cookie in iter(self):
        for cookie in self.items():
            if ((domain is None or cookie.domain == domain) and (path is None or cookie.path == path)):
                dictionary[cookie.name] = cookie.value
        return dictionary

    def add_opt(self, opt):
        # if opt not in self.curl_session.extra_opts:
        if DEBUG:
            print('opt', opt)
        self.curl_session.extra_opts.append(opt)

    def set(
        self, name=None, value=None, domain=None, path='/', expires=None, flag=False, secure=False, http_only=False
    ):
        assert name is not None
        assert value is not None
        assert domain is not None

        with self.curl_session.cookie_lock:
            # support client code that unsets cookies by assignment of a None value:
            if value is None:
                # self.remove_cookie_by_name(name, domain=kwargs.get('domain'), path=kwargs.get('path'))
                self.remove_cookie_by_name(name, domain=domain, path=path)
                return

            cookie = Cookie(
                domain=domain,
                flag=str_to_bool(flag),
                path=path,
                expires=expires,
                name=name,
                value=value,
                rest={'HttpOnly': http_only if http_only else None},
                secure=str_to_bool(secure)
            )
            self._cookies.append(cookie)

            opt = (
                pycurl.COOKIELIST, "{domain}\t{flag}\t{path}\t{secure}\t{expires}\t{name}\t{value}".format(
                    domain=cookie.domain,
                    path=cookie.path,
                    name=cookie.name,
                    value=cookie.value,
                    flag='TRUE' if cookie.flag else 'FALSE',
                    secure='TRUE' if cookie.secure else 'FALSE',
                    expires='0' if cookie.expires is None else cookie.expires
                )
            )
            self.add_opt(opt)

    def renew_previously_removed(self, cookie):
        """If a cookie gets removed before with .clear() then we save that cookie in _removed_cookies_dict
        so that when the website sets that same cookie again it will work, whereas if we don't do that then
        our cookie.expires=1 will prevent the new cookie from being set.

        We set cookie.expire=1 to "remove" the cookie from curl using its Netscape cookie format.
        This overwrites the website's cookies, so if a website sends Set-Cookie after we clear it,
        then we have to manually un-expire (renew) the same cookie domain/path/name.
        """

        try:
            old_cookie = self._removed_cookies_dict[cookie.domain][cookie.path][cookie.name]
            assert old_cookie
        except KeyError:
            # new cookie, never seen before.
            return

        if (old_cookie.expires == 1 or old_cookie.expires == 'Thu, 01 Jan 1970 00:00:01 GMT'):
            self.set(
                name=cookie.name,
                value=cookie.value,
                domain=cookie.domain,
                path=cookie.path,
                expires=cookie.expires,
                flag=cookie.flag,
                secure=cookie.secure,
                http_only=cookie._rest['HttpOnly']
            )
            self.store_removed_cookie(cookie)

    def clear(self, domain=None, path=None, name=None):
        # https://github.com/python/cpython/blob/80097e089ba22a42d804e65fbbcf35e5e49eed00/Lib/http/cookiejar.py#L1684
        # Expires=1 is the same as Expires=Thu, 01 Jan 1970 00:00:01 GMT
        """Clear some cookies.
        Invoking this method without arguments will clear all cookies.  If
        given a single argument, only cookies belonging to that domain will be
        removed.  If given two arguments, cookies belonging to the specified
        path within that domain are removed.  If given three arguments, then
        the cookie with the specified name, path and domain is removed.
        Raises KeyError if no matching cookie exists.
        """

        if name is not None:
            # if (domain is None) or (path is None):
            #   raise ValueError(
            #       "domain and path must be given to remove a cookie by name")
            cookie = self.find_cookie(domain=domain, path=path, name=name)
            if cookie:
                assert cookie.name == name
                cookie.expires = 1
                if domain: assert cookie.domain == domain
                if path: assert cookie.path == path

                self._cookies.remove(cookie)
                self.store_removed_cookie(cookie)
                if DEBUG:
                    print(cookie)
                if domain.startswith('.'):
                    domain = domain[1:]
                # opt = (pycurl.COOKIELIST, "Set-Cookie: {name}={value}; Expires=Thu, 01 Jan 1970 00:00:01 GMT; domain={domain}; Path={path}".format(domain=domain, path=path, name=name, value=value))
                opt = (
                    pycurl.COOKIELIST, "{domain}\t{flag}\t{path}\t{secure}\t{expires}\t{name}\t{value}".format(
                        domain=domain,
                        path=cookie.path,
                        name=cookie.name,
                        value=cookie.value,
                        flag='TRUE' if cookie.flag else 'FALSE',
                        secure='TRUE' if cookie.secure else 'FALSE',
                        expires=cookie.expires
                    )
                )
                self.add_opt(opt)
        elif path is not None:
            if domain is None:
                raise ValueError("domain must be given to remove cookies by path")
            # clear all cookies for that path
            for cookie in self._cookies[:]:
                if cookie.domain == domain and cookie.path == path:
                    cookie.expires = 1
                    self._cookies.remove(cookie)
                    self.store_removed_cookie(cookie)
                    if domain.startswith('.'):
                        domain = domain[1:]
                    # opt = (pycurl.COOKIELIST, "Set-Cookie: {name}={value}; Expires=Thu, 01 Jan 1970 00:00:01 GMT; domain={domain}; Path={path}".format(domain=domain, path=cookie.path, name=cookie.name, value=cookie.value))
                    opt = (
                        pycurl.COOKIELIST, "{domain}\t{flag}\t{path}\t{secure}\t{expires}\t{name}\t{value}".format(
                            domain=domain,
                            path=cookie.path,
                            name=cookie.name,
                            value=cookie.value,
                            flag='TRUE' if cookie.flag else 'FALSE',
                            secure='TRUE' if cookie.secure else 'FALSE',
                            expires=cookie.expires
                        )
                    )
                    self.add_opt(opt)
        elif domain is not None:
            # clear all cookies for that domain
            for cookie in self._cookies[:]:
                if cookie.domain == domain:
                    cookie.expires = 1
                    self._cookies.remove(cookie)
                    self.store_removed_cookie(cookie)
                    if domain.startswith('.'):
                        domain = domain[1:]
                    # opt = (pycurl.COOKIELIST, "Set-Cookie: {name}=eee{value}; Expires=Thu, 01 Jan 2030 00:00:01 GMT; domain={domain}; Path={path}".format(domain=domain, path=cookie.path, name=cookie.name, value=cookie.value))
                    # self.add_opt(opt)
                    # opt = (pycurl.COOKIELIST, "Set-Cookie: {name}=fff{value}; Expires=Thu, 01 Jan 2030 00:00:01 GMT; domain={domain}; Path={path}{secure}{http_only}".format(domain=domain, path=cookie.path, name=cookie.name, value=cookie.value, secure='; secure' if cookie.secure else '', http_only='; httponly' if cookie._rest['HttpOnly'] else ''))
                    opt = (
                        pycurl.COOKIELIST, "{domain}\t{flag}\t{path}\t{secure}\t{expires}\t{name}\t{value}".format(
                            domain=domain,
                            path=cookie.path,
                            name=cookie.name,
                            value=cookie.value,
                            flag='TRUE' if cookie.flag else 'FALSE',
                            secure='TRUE' if cookie.secure else 'FALSE',
                            expires=cookie.expires
                        )
                    )
                    self.add_opt(opt)
                    # opt = (pycurl.COOKIELIST, "httpbin.org\tTRUE\t/\tFALSE\t0\tfoo\t111111111".format(domain=domain, path=cookie.path, name=cookie.name, value=cookie.value))
                    # self.add_opt(opt)
        else:
            for cookie in self._cookies[:]:
                cookie.expires = 1
                # self._cookies.remove(cookie)
                self.store_removed_cookie(cookie)

            self._cookies = []
            opt = (
                pycurl.COOKIELIST, 'ALL'
            )  # can be used to clear all the cookies https://curl.haxx.se/libcurl/c/CURLOPT_COOKIELIST.html
            self.add_opt(opt)

    def store_removed_cookie(self, cookie):
        if cookie.domain not in self._removed_cookies_dict:
            self._removed_cookies_dict[cookie.domain] = {}
        if cookie.path not in self._removed_cookies_dict[cookie.domain]:
            self._removed_cookies_dict[cookie.domain][cookie.path] = {}
        self._removed_cookies_dict[cookie.domain][cookie.path][cookie.name] = cookie

    def clear_expired_cookies(self):
        with self.curl_session.cookie_lock:
            now = time.time()
            for cookie in self._cookies[:]:
                if cookie.is_expired(now):
                    # self._cookies.remove(cookie)
                    self.clear(cookie.domain, cookie.path, cookie.name)

    def keep_whitelisted_cookies(self):
        with self.curl_session.cookie_lock:
            for cookie in self._cookies[:]:
                if not self.is_cookie_whitelisted(cookie):
                    # self._cookies.remove(cookie)
                    self.clear(cookie.domain, cookie.path, cookie.name)

    def is_cookie_whitelisted(self, cookie):
        return any(cookie.name == name for name in self.cookie_whitelist)

    def find_cookie(self, domain=None, path=None, name=None):
        """ Finds and returns the cookie as opposed to _find_no_duplicates that only returns the cookie value
        :param name: a string containing name of cookie
        :param domain: (optional) string containing domain of cookie
        :param path: (optional) string containing path of cookie
        # :raises KeyError: if cookie is not found
        # :raises CookieConflictError: if there are multiple cookies
            that match name and optionally domain and path
        :return: cookie
        """
        toReturn = None
        for cookie in iter(self):
            if cookie.name == name:
                if domain is None or cookie.domain == domain:
                    if path is None or cookie.path == path:
                        if toReturn is not None:  # if there are multiple cookies that meet passed in criteria
                            raise requests.cookies.RequestsCookieJar.CookieConflictError(
                                'There are multiple cookies with name, %r' % (name)
                            )
                        toReturn = cookie  # we will eventually return this as long as no cookie conflict

        if toReturn:
            return toReturn
        return None

    def pop(self, name):
        cookie = self.find_cookie(name=name)
        if cookie is None:
            raise KeyError('name=%r' % name)
        # cookie = self._find_no_duplicates(name=name)
        self.clear(cookie.domain, cookie.path, cookie.name)
        return cookie.value

    def __contains__(self, name):
        try:
            if self.find_cookie(name=name):
                return True
            else:
                return False
        except requests.cookies.RequestsCookieJar.CookieConflictError:
            return True

    def __len__(self):
        return len(self._cookies)

    def __repr__(self):
        return str(self._cookies)

    def __iter__(self):
        if DEBUG:
            print('iter')
        #yield from self._cookies  # py 3
        for x in self._cookies:
            yield x


# class CurlSession(object):
class Session(object):
    __attrs__ = [
        'headers',
        'cookies',
        'auth',
        'proxies',
        'params',
        'verify',
        'trust_env',
        'max_redirects',
    ]

    req_count = 0
    req_id = 0

    # num_handles = 0
    def __init__(
        self,
        headers=None,
        data=None,
        params=None,
        json=None,
        proxies=None,
        verify=None,
        allow_redirects=True,
        max_redirects=30,
        max_retries=10,
        pool_connections=10,
        pool_maxsize=10,
        use_cookies=True,
        cookie_whitelist=None,
        pipewait=False,
        verbose=None,
        block=True,
        ventilator=False
    ):
        # TODO: trust_env
        if not headers:
            headers = {
                'User-Agent': pycurl.version,
                'Accept': '*/*',
                'Accept-Encoding': 'deflate, gzip, br' if 'brotli' in pycurl.version else 'deflate, gzip'
            }

        self.headers = headers or {}
        self.params = params or {}
        self.proxies = proxies
        # self.proxy = get_proxy(proxies)
        self.verify = verify

        self.allow_redirects = allow_redirects
        self.max_redirects = max_redirects
        self.max_retries = max_retries
        self.block = block
        self.ventilator = ventilator

        if not pool_connections and not pool_maxsize:
            self.pool_connections = 10
            self.pool_maxsize = 10
        elif pool_connections and (not pool_maxsize or (pool_connections >= pool_maxsize)):
            self.pool_connections = self.pool_maxsize = pool_connections
        else:
            self.pool_connections = self.pool_maxsize = pool_maxsize

        self.extra_opts = []
        self.cookies = Cookies(self)

        self.multi_queue = queue.Queue()
        # self.multi_info_task_queue = queue.Queue()
        self.perform_lock = threading.Lock()
        self.cookie_lock = threading.Lock()
        self.lock = threading.Lock()
        self.curl_dict_lock = threading.Lock()
        self.concurrency_semaphore = threading.BoundedSemaphore()

        self.use_cookies = use_cookies
        self.cookie_whitelist = cookie_whitelist
        # if cookie_whitelist:
        #   self.cookie_whitelist = cookie_whitelist
        # else:
        #   self.cookie_whitelist = []

        # TODO: make a global and check once on the first curl.
        self.pipewait_supprted = True
        self.tcp_fastopen_supported = True

        self.pipewait = pipewait
        if verbose is None:
            self.verbose = VERBOSE
        else:
            self.verbose = verbose

        self.share = pycurl.CurlShare()
        self.share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_DNS)  # default in multi
        self.share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_SSL_SESSION)
        if self.use_cookies:
            self.share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_COOKIE)

        self.setup_curl_multi()

        self.curl_storage = None  # list
        self.free_handle_queue = queue.Queue()
        # self.setup_curls()
        self.reuse_handles = True
        self.curl_dict = OrderedDict()

        self.lazy_load_curls = True
        self.use_curlmulti_thread = False
        if self.use_curlmulti_thread:
            if self.lazy_load_curls:
                # # don't spawn the thread right away to avoid opening many threads when many curl_session objects are created, but only one at a time is used.
                self.thread = None
            else:
                with self.lock:
                    with self.perform_lock:
                        self.setup_curls()
                        self.thread = threading.Thread(target=self._perform, name='MultiPerformer')
                        self.thread.daemon = True
                        self.thread.start()
        else:
            self.thread = None
            if not self.lazy_load_curls:
                with self.lock:
                    with self.perform_lock:
                        self.setup_curls()

    def setup_curl_multi(self):
        self.m = pycurl.CurlMulti()
        self.m.setopt(pycurl.M_PIPELINING, 2)

    def setup_curls(self):
        if self.pool_connections >= 1000:
            print(
                'WARNING: setting pool_connections to {} will create as many curl handles, potentially using more memory than necessary unless you really need to send this many requests simultaneously.'
                .format(self.pool_connections)
            )

        self.curl_storage = [pycurl.Curl() for x in range(self.pool_connections)]
        for curl in self.curl_storage:
            curl.setopt(pycurl.SHARE, self.share)
            # self.share.setopt(pycurl.SH_UNSHARE, pycurl.LOCK_DATA_COOKIE)
            # self.share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_COOKIE)
            curl.lock = self.lock
            curl.curl_dict_lock = self.curl_dict_lock
            self.free_handle_queue.put(curl)

    def clear_curls(self):
        # clear up the curls lists and queue
        self.curl_storage = None  # list
        while 1:
            try:
                removed_curl = self.free_handle_queue.get_nowait()
                removed_curl.close()
                del removed_curl
            except queue.Empty:
                break
        # print('cleared')

    def get(
        self,
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        block=None,
        ventilator=None
    ):
        return self.request(
            method='GET',
            url=url,
            headers=headers,
            data=data,
            params=params,
            json=json,
            allow_redirects=allow_redirects,
            timeout=timeout,
            proxies=proxies,
            verify=verify,
            block=block,
            ventilator=ventilator
        )

    def head(
        self,
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        block=None,
        ventilator=None
    ):
        return self.request(
            method='HEAD',
            url=url,
            headers=headers,
            data=data,
            params=params,
            json=json,
            allow_redirects=allow_redirects,
            timeout=timeout,
            proxies=proxies,
            verify=verify,
            block=block,
            ventilator=ventilator
        )

    def post(
        self,
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        block=None,
        ventilator=None
    ):
        return self.request(
            method='POST',
            url=url,
            headers=headers,
            data=data,
            params=params,
            json=json,
            allow_redirects=allow_redirects,
            timeout=timeout,
            proxies=proxies,
            verify=verify,
            block=block,
            ventilator=ventilator
        )

    def request(
        self,
        method='GET',
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        block=None,
        ventilator=None
    ):
        block = merge_setting(block, self.block)

        if not block:
            self.use_curlmulti_thread = True

        if DEBUG:
            print('add request', url)
        curl = self._add_request(
            method=method,
            url=url,
            headers=headers,
            data=data,
            params=params,
            json=json,
            allow_redirects=allow_redirects,
            timeout=timeout,
            proxies=proxies,
            verify=verify,
            block=block,
            ventilator=ventilator
        )
        if DEBUG:
            print('got curl', curl, curl.request.url)

        if not block:
            return curl.request
        else:
            # r = self.result(curl_session, curl.request)
            r = curl.request.result()
            if DEBUG:
                print('got response', r, r.url)
            return r

    def _add_request(
        self,
        method='GET',
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        post_fields=None,
        block=None,
        ventilator=None
    ):
        if self.curl_storage is None:
            # create the curls
            # if use_curlmulti_thread then the curls expire after 300s of inactivity (no requests).
            with self.lock:
                if self.curl_storage is None:
                    self.clear_curls()  # clear again just in case some curls are still in queue after the proper clear
                    self.setup_curls()

        with self.lock:
            self.req_count += 1
            self.req_id += 1
            req_id = self.req_id

        curl = self._create_curl(
            method=method,
            url=url,
            headers=headers,
            data=data,
            params=params,
            json=json,
            allow_redirects=allow_redirects,
            timeout=timeout,
            proxies=proxies,
            verify=verify,
            post_fields=post_fields,
            block=block,
            ventilator=ventilator,
            req_id=req_id
        )

        with self.lock:
            # without lock throws "cannot add/remove handle - multi_perform() already running"
            # waits for one of the (quick) .perform() calls to finish and release lock before adding another curl request.
            # [DONE] it would probably be faster to have a set amount of handles (i.e. the pool_connections int) and unset & reuse them like in the tornado example.
            # self.num_handles += 1
            self.m.add_handle(curl)

        # self.multi_queue.put(1)
        self.multi_queue.put(
            req_id
        )  # doesn't matter what we put into the queue, even 1 will work, but req_id makes it easier to debug.
        if self.use_curlmulti_thread:
            if self.thread is None:
                # spawn the curlmulti thread.
                # the thread expires after 300s of inactivity (no requests).
                with self.lock:
                    if self.thread is None:
                        self.thread = threading.Thread(target=self._perform, name='MultiPerformer')
                        self.thread.daemon = True
                        self.thread.start()
        else:
            self._perform(performer_curl=curl)
        return curl

    def curl_to_requests(self, curl, url=None, body=None):
        # r.is_curl = True

        r = curl.r
        r.request = curl.request

        with self.lock:
            # GETINFO

            # https://curl.haxx.se/libcurl/c/CURLINFO_HTTP_VERSION.html
            version_name = curl.getinfo(pycurl.INFO_HTTP_VERSION)
            if version_name == pycurl.CURL_HTTP_VERSION_1_0:
                r.getinfo.version = 'HTTP/1.0'
            elif version_name == pycurl.CURL_HTTP_VERSION_1_1:
                r.getinfo.version = 'HTTP/1.1'
            elif version_name == getattr(pycurl, 'CURL_HTTP_VERSION_2_0', None):
                r.getinfo.version = 'HTTP/2.0'
            elif not version_name:
                # r.getinfo.version = 'unknown'
                # Get the HTTP version by parsing the headers
                print("Can't determine the http version from curl, parsing the headers instead")
                r.headers
                if not r.getinfo.version:
                    raise ValueError("Can't determine the http version from headers, shouldn't happen.")
            else:
                raise ValueError("Can't determine the http version from curl, shouldn't happen.")

            # r._curl.total_time = curl.getinfo(curl.TOTAL_TIME)
            r.getinfo.total_time = curl.getinfo(curl.TOTAL_TIME)
            # r.elapsed = Elapsed(r._curl.total_time)
            r.elapsed = Elapsed(r.getinfo.total_time)

            # r._status_code = int(curl.getinfo(pycurl.HTTP_CODE))
            r.status_code = int(curl.getinfo(pycurl.HTTP_CODE))
            r.url = curl.getinfo(pycurl.EFFECTIVE_URL)
            # r.elapsed = curl.getinfo(curl.TOTAL_TIME)

        # r.content = r._content = body
        r._body = body
        if DEBUG:
            print('r.is_redirect', r.is_redirect)

        r.cookies = []  # TODO
        if self.use_cookies:
            self.cookies._cookies = parse_cookie(curl=curl, curl_session=self)

        if self.cookie_whitelist:
            self.cookies.keep_whitelisted_cookies()
        if DEBUG:
            print('')
            # print('self.cookies._cookies', self.cookies._cookies)
            print('')

        # if DEBUG:
        #   for cookie in self.cookies:
        #       print('cookie', cookie)
        #       print('cookie.name', cookie.name)
        #       print('cookie.value', cookie.value)
        #       print('cookie.is_expired', cookie.is_expired())

        return r

    def _perform(self, performer_curl=None):
        # do perform only if not already in progress from another thread.
        free_lock = self.perform_lock.acquire(False)
        if free_lock is False: return

        while 1:  # thread loop
            if self.use_curlmulti_thread:
                try:
                    if DEBUG and self.use_curlmulti_thread: assert threading.current_thread().name == 'MultiPerformer'
                    _ = self.multi_queue.get(
                        timeout=300 if not DEBUG else 8
                    )  # TODO: read system's keepalive instead and if larger than 300 then use that keepalive+3
                except queue.Empty:
                    # turn off the thread after inactivity. will spawn a new one if more requests come in later.
                    self.thread = None

                    self.clear_curls()
                    self.perform_lock.release()
                    return
            else:
                try:
                    _ = self.multi_queue.get_nowait()
                except queue.Empty:
                    # no more requests to process
                    self.perform_lock.release()
                    return

            # if _ == 1:
            if isinstance(_, six.integer_types):
                # perform request
                req_id = _
                if DEBUG:
                    print('perform :: asked to m.perform() from req_id: {}'.format(req_id))
            else:
                result_id, curl, task_type, task_command = _

                if DEBUG:
                    print(
                        'perform :: asekd to curl.getinfo() from result_id: {}, curl: {}, task_type: {}, task_command: {}:'
                        .format(result_id, curl, task_type, task_command)
                    )

                if task_type == 'getinfo':
                    getinfo_result = curl.getinfo(task_command)
                    multi_info_task_queue_results[result_id]['queue'].put(getinfo_result)
                    curl = None
                    continue
                else:
                    raise ValueError('unknown perform task')

            # while 1:
            #   with self.lock:
            #       ret, num_handles = self.m.perform()
            #   if ret != pycurl.E_CALL_MULTI_PERFORM: break
            num_handles = len(self.curl_dict)
            last_num_handles = num_handles

            while num_handles:
                if DEBUG:
                    print('select 1:')
                ret = self.m.select(1.0)
                if DEBUG:
                    print('selected 1:', ret)
                if ret == -1: continue

                while 1:
                    with self.lock:
                        ret, num_handles = self.m.perform()

                    if DEBUG:
                        print('ret, num_handles', ret, num_handles)
                        print(ret, num_handles)

                    # signal the newest request (one added most recently) to do the perform
                    # instead of calling perform from the same thread over and over again,
                    # which could make it return late or never if new requests keep getting added.
                    # with this we can avoid spawning a dedicated thread to call the perform indefinitely.
                    # for example, instead of having 10 ongoing requests and the #1 request calling perform
                    # until all requests finish so that #1 returns last, pass the perform task to the newest thread,
                    # so that the #1 request can return early while the #10 request continues to call perform.
                    if not self.use_curlmulti_thread and num_handles != last_num_handles and num_handles >= 2:
                        assert performer_curl is not None
                        last_num_handles = num_handles
                        with self.curl_dict_lock:
                            newest_curl_uuid, newest_req_v = get_newest_dict_item(
                                self.curl_dict
                            )  # (curl.id, {'queue: queue, 'curl': curl})
                        if performer_curl.id != newest_curl_uuid:
                            # task the newest thread with performing
                            newest_req_v['queue'].put('perform')
                            if DEBUG:
                                print(
                                    'swapped curl performer from {} to {}'.format(performer_curl.id, newest_curl_uuid)
                                )
                            self.perform_lock.release()
                            return

                    if ret != pycurl.E_CALL_MULTI_PERFORM: break

                # Check for curl objects which have terminated, and add them to the freelist
                while 1:
                    num_q, ok_list, err_list = self.m.info_read()
                    if DEBUG:
                        print('num_q, ok_list, err_list', num_q, ok_list, err_list)
                    for curl in ok_list:
                        if DEBUG:
                            print(
                                'perform :: got success curl id: {} from req_id: {} | req_id doesn\'t have to match curl id as it\'s just a req_id asking to m.perform()'
                                .format(curl.id, req_id)
                            )

                        block = self.curl_dict[curl.id]['block']
                        ventilator = self.curl_dict[curl.id]['ventilator']
                        if not block:
                            request = self.curl_dict[curl.id]['request']
                            response = self.curl_dict[curl.id]['response']
                            if ventilator:
                                self.ventilator_cleanup(curl, request, response)
                                remove_handle(self, curl, request, response)
                            else:
                                # If the curl request is non-blocking then we have to call process_result() to get all curl's properties e.g. call curl.getinfo(pycurl.EFFECTIVE_URL),
                                # because that curl handle will no longer be available for that request/response object after we reuse it for another request.
                                # For example, if we have 10 curl handles and start 15 block=False requests, then the first 10 requests will get a fresh curl handle each,
                                # but then the remaining 5 requests will reuse the same curl handles. Because of that, the _perform() thread has to call request.process_result()
                                # as soon as the request finishes, before we add mark its curl handle as free and add it back to the free_handle_queue queue for new requests to reuse.
                                # We only call request.process_result() from within the _perform when block=False, because on block=True, request.result() is called automatically from the user's main/other application threads and that way we don't overburden the MultiPerformer thread that we spawn once per session if block=False is used.
                                request.process_result('success', curl, request, response, None, None)

                        if not ventilator:
                            self.curl_dict[curl.id]['queue'].put(('success', None, None))
                        # freelist.append(curl)
                    for curl, errno, errmsg in err_list:
                        if DEBUG:
                            print(
                                'perform :: got error curl id: {} from req_id: {} | req_id doesn\'t have to match curl id as it\'s just a req_id asking to m.perform()'
                                .format(curl.id, req_id)
                            )

                        block = self.curl_dict[curl.id]['block']
                        if not block:
                            request = self.curl_dict[curl.id]['request']
                            response = self.curl_dict[curl.id]['response']
                            request.process_result('error', curl, request, response, errno, errmsg)

                        self.curl_dict[curl.id]['queue'].put(('error', errno, errmsg))
                        # freelist.append(curl)
                    if DEBUG:
                        num_processed = len(ok_list) + len(err_list)
                        print('num_processed', num_processed)
                    if num_q == 0:
                        break

            # Currently no more I/O is pending, could do something in the meantime
            # (display a progress bar, etc.).
            # We just call select() to sleep until some more data is available.
            if self.use_curlmulti_thread:
                if DEBUG:
                    print('select 2')
                self.m.select(1.0)
                if DEBUG:
                    print('selected 2')

        self.perform_lock.release()

    # def read_all(self):
    #     for curl, response in self.curl_storage:
    #         print(curl, curl.getinfo(pycurl.HTTP_CODE), curl.getinfo(pycurl.EFFECTIVE_URL), 'RESPONSE:', response.getvalue()[:50]) # this does nothing --prints blank lines

    def close(self, force_close=False):
        """Close CurlMulti and all the curls.
        after closed, throws "pycurl.error: cannot add/remove handle - multi-stack is closed",
        however in Requests one can call Session.close() and still continue sending requests on that session,
        because in Requests it's just the connections that get closed, but in CurlMulti the session gets destroyed,
        so we recreate CurlMulti along with the curls.
        Not thread safe, it's very likely to throw errors (e.g. "cannot invoke getinfo() - no curl handle") for the running requests if the session gets closed in multithreading.

        Close the session only if it's not being multithreading with multiple requests to random errors on the ongoing requests.
        """
        with self.lock:
            if self.req_count == 0 or force_close:
                # close all curls
                if self.curl_storage:
                    for curl in self.curl_storage:
                        curl.close()

                # empty the free curl queue
                self.clear_curls()

                # close CurlMulti
                self.m.close()

                # all closed, now re-create.
                self.setup_curl_multi()
                self.curl_dict = OrderedDict()
                self.setup_curls()
        return

    def ventilator_cleanup(self, curl, request, response):
        if response._headers_output is not None:
            response._headers_output.close()
            response._headers_output = None

        if response._body_output is not None:
            response._body_output.close()
            response._body_output = None

        assert request is curl.request  # temp
        if request is curl.request:
            del curl.r
            del curl.request
            with self.curl_dict_lock:
                del self.curl_dict[request.id]

    def _create_curl(
        self,
        method='GET',
        url=None,
        headers=None,
        data=None,
        params=None,
        json=None,
        allow_redirects=None,
        timeout=60,
        proxies=None,
        verify=None,
        post_fields=None,
        block=None,
        ventilator=None,
        req_id=None
    ):
        # curl = pycurl.Curl()

        try:
            # timeout=(3.05, 27)
            connect_timeout, read_timeout = timeout
            timeout = max(connect_timeout, read_timeout)
        except TypeError:
            # timeout=60
            connect_timeout = read_timeout = timeout

        try:
            curl = self.free_handle_queue.get(timeout)
            if DEBUG:
                print('free_handle_queue', req_id)
        except queue.Empty:
            raise translate_curl_exception(
                pycurl.E_OPERATION_TIMEDOUT,
                '[No free curl handle, try to increase pool_connections] Operation timed out after {} milliseconds'.
                format(timeout * 1000)
            )

        # Re-initializes all options previously set on a specified CURL handle to the default values. This puts back the handle to the same state as it was in when it was just created with curl_easy_init.
        # It does not change the following information kept in the handle: live connections, the Session ID cache, the DNS cache, the cookies and shares.
        curl.reset()

        # Create the Request.
        curl.request = CurlRequest(
            method=method.upper(),
            url=url,
            headers=headers,
            # files=files,
            data=data or {},
            json=json,
            params=params or {},
            # auth=auth,
            # cookies=cookies,
            # hooks=hooks,
        )
        curl.request.curl_session = self
        # curl.request._curl = curl
        curl.request.pending = True

        # curl.id = uuid.uuid4().hex
        if DEBUG:
            print('create req_id', req_id)
        curl.request.id = req_id  # increments
        curl.id = curl.request.id

        # TODO: auth, cookies, files https://github.com/kennethreitz/requests/blob/eaab47f033cae3bd443abb6707dfc30ae3482c36/requests/sessions.py#L460
        headers = merge_setting(curl.request.headers, self.headers, dict_class=CaseInsensitiveDict)
        params = merge_setting(curl.request.params, self.params)
        allow_redirects = merge_setting(allow_redirects, self.allow_redirects)

        block = merge_setting(block, self.block)
        ventilator = merge_setting(ventilator, self.ventilator)
        if ventilator:
            block = False

        # settings
        verify = merge_setting(verify, self.verify)
        proxies = merge_setting(proxies, self.proxies)
        curl.proxy = get_proxy(proxies)

        curl.r = CurlResponse(request=curl.request)
        # curl.r._curl = curl  # a user shouldn't look at the curl object because it's going to get reused by another request.

        # curl.url = url
        # curl.headers = headers
        # curl.allow_redirects = allow_redirects
        # curl.timeout = timeout
        curl.request.url = url
        # curl.request.headers = headers
        curl.request.allow_redirects = allow_redirects
        curl.request.timeout = timeout

        #

        curl.post_fields = post_fields

        curl.r._headers_output = BytesIO()
        curl.r._body_output = BytesIO()

        # self.curl_storage.append([curl, output, curl.id])
        # self.curl_storage.append(curl)
        # with self.lock:
        with self.curl_dict_lock:
            self.curl_dict[curl.id] = {
                'queue': queue.Queue(),
                'curl': curl,
                'request': curl.request,
                'response': curl.r,
                'block': block,
                'ventilator': ventilator
            }

        # TODO: if curl handles are reused instead of recreated then use reset on each handle:
        # http://pycurl.io/docs/latest/curlobject.html#pycurl.Curl.reset
        # Reset all options set on curl handle to default values, but preserves live connections, session ID cache, DNS cache, cookies, and shares.
        # Corresponds to curl_easy_reset in libcurl.

        curl.setopt(CURL_OPTIONS[method], True)

        # https://github.com/lorien/grab/blob/master/grab/transport/curl.py
        url = requote_uri(url)
        url = make_str(url)

        if params:
            assert b'?' not in url
            curl.setopt(curl.URL, url + b'?' + make_str(urlencode(params)))
        else:
            curl.setopt(curl.URL, url)

        if data is not None:
            assert method == 'POST' or method == 'PUT'

            # https://curl.haxx.se/libcurl/c/CURLOPT_POSTFIELDSIZE.html
            # TODO: If you post more than 2GB, use CURLOPT_POSTFIELDSIZE_LARGE.
            curl.setopt(pycurl.POST, 1)
            if isinstance(data, basestring):
                data_encoded = data
            elif MultipartEncoder is not None and isinstance(data, MultipartEncoder):
                data_encoded = data.to_string()
            else:
                data_encoded = urlencode(data)

            curl.setopt(pycurl.POSTFIELDS, data_encoded)
        elif json is not None:
            assert method == 'POST' or method == 'PUT'
            if 'application/json' not in headers.get('Content-Type', ''):
                headers['Content-Type'] = 'application/json;charset=UTF-8'

            json = complexjson.dumps(json, ensure_ascii=False)  # indent=2,
            curl.setopt(pycurl.POST, 1)
            curl.setopt(pycurl.POSTFIELDS, json)

        curl.setopt(curl.WRITEHEADER, curl.r._headers_output)
        curl.setopt(curl.WRITEDATA, curl.r._body_output)

        if headers:
            # curl.setopt(pycurl.ENCODING, "")  # don't use, sometimes throws "[#61]: Unrecognized content encoding type. libcurl understands deflate, gzip, br content encodings."
            accept_encoding = headers.get('Accept-Encoding', '')
            if BR_SUPPORT is False and 'br' in accept_encoding:
                # Remove brotli encoding if it's an old curl (<7.57.0) that doesn't support it.
                # "gzip, deflate, br" --> "gzip, deflate"
                accept_encoding = BR_REGEX.sub("", accept_encoding)
                headers['Accept-Encoding'] = accept_encoding
            curl.setopt(pycurl.ENCODING, accept_encoding)

            # https://curl.haxx.se/docs/todo.html#Rearrange_request_header_order
            HTTPHEADER = ['{}: {}'.format(k, v) for k, v in headers.items() if v is not None]
            if method == 'POST' or method == 'PUT':
                HTTPHEADER.append('Expect: ')  # remove this header
            curl.setopt(pycurl.HTTPHEADER, HTTPHEADER)
            try:
                curl.setopt(pycurl.USERAGENT, headers['User-Agent'])
            except KeyError:
                pass

        if verify:
            curl.setopt(pycurl.SSL_VERIFYPEER, 1)
            curl.setopt(pycurl.SSL_VERIFYHOST, 2)
            curl.setopt(pycurl.CAINFO, certifi.where())
        else:
            curl.setopt(pycurl.SSL_VERIFYPEER, 0)
            curl.setopt(pycurl.SSL_VERIFYHOST, 0)

        curl.setopt(pycurl.FOLLOWLOCATION, 1 if allow_redirects else 0)
        curl.setopt(pycurl.MAXREDIRS, self.max_redirects)
        # curl.setopt(pycurl.CONNECTTIMEOUT, 30)
        # curl.setopt(pycurl.TIMEOUT, 300)
        curl.setopt(pycurl.CONNECTTIMEOUT, int(connect_timeout))
        # curl.setopt(pycurl.TIMEOUT, int(read_timeout))
        curl.setopt(pycurl.TIMEOUT_MS, int(float(read_timeout) * 1000))
        # curl.setopt(pycurl.MAXCONNECTS, 10)
        curl.setopt(pycurl.MAXCONNECTS, self.pool_connections)
        curl.setopt(pycurl.NOSIGNAL, 1)
        # if VERBOSE:
        if self.verbose:
            curl.setopt(pycurl.VERBOSE, True)  # temp disabled
            # curl.setopt(pycurl.NOPROGRESS, 0)  # not used regardless
        else:
            curl.setopt(pycurl.NOPROGRESS, 1)
        if curl.proxy:
            curl.setopt(pycurl.PROXY, curl.proxy)
            curl.setopt(pycurl.NOPROXY, 'localhost,127.0.0.1,0.0.0.0,::1')
        # curl.setopt(pycurl.COOKIE, 'a=a')
        # curl.setopt(pycurl.COOKIELIST, 'a=a')
        # curl.setopt(pycurl.COOKIELIST, '')

        # if not self.use_cookies:
        #   # not needed unless curl handle gets reused, which it's not atm
        #   curl.setopt(pycurl.COOKIELIST, 'ALL')  # can be used to clear the cookies https://curl.haxx.se/libcurl/c/CURLOPT_COOKIELIST.html

        # curl.setopt(pycurl.COOKIEFILE, 'cookie_1.txt')
        # curl.setopt(pycurl.COOKIEJAR, 'cookie_1.txt')
        # curl.setopt(pycurl.COOKIEJAR, 'cookie_2.txt')

        # CURLOPT_COOKIE adds the given string to the outgoing Cookie: header in the request, it
        # DOES NOT add any cookies to libcurl's internal list of cookies. Use
        # CURLOPT_COOKIELIST for that. You can in fact also use CURLOPT_COOKIELIST to
        # "delete" a cookie by simply setting it again with an expired time or similar.

        # both Expires format work
        # doesn't add the cookie if no domain is specified.
        # curl.setopt(pycurl.COOKIELIST, "Set-Cookie: asdf=aaaaa; Expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=kith.com; Path=/")
        # curl.setopt(pycurl.COOKIELIST, "Set-Cookie: asdf=aaaaa; Expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=kith.com; Path=/")

        # curl.setopt(pycurl.COOKIELIST, "Set-Cookie: 3=4")
        # curl.setopt(pycurl.COOKIELIST, "Set-Cookie: 5=6")

        # HTTP_VERSION
        # https://curl.haxx.se/libcurl/c/CURLOPT_HTTP_VERSION.html
        # CURL_HTTP_VERSION_2_0:
        # Attempt HTTP 2 requests. libcurl will fall back to HTTP 1.1 if HTTP 2 can't be negotiated with the server. (Added in 7.33.0)
        # The alias CURL_HTTP_VERSION_2 was added in 7.43.0 to better reflect the actual protocol name.
        try:
            curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2_0)
        except pycurl.error as e:
            # HTTP2 not supported in this version of pycurl or curl. Will send HTTP1.1 requests instead.
            assert e.args[0] is pycurl.E_UNSUPPORTED_PROTOCOL

        # CURLOPT_PIPEWAIT - wait for pipelining/multiplexing
        # https://curl.haxx.se/libcurl/c/CURLOPT_PIPEWAIT.html
        # https://curl.haxx.se/docs/http2.html
        # While libcurl sets up a connection to a HTTP server there is a period during which it doesn't know if it can pipeline or do multiplexing and if you add new transfers in that period, libcurl will default to start new connections for those transfers. With the new option CURLOPT_PIPEWAIT (added in 7.43.0), you can ask that a transfer should rather wait and see in case there's a connection for the same host in progress that might end up being possible to multiplex on. It favours keeping the number of connections low to the cost of slightly longer time to first byte transferred.
        # Default: disabled
        if self.pipewait and self.pipewait_supprted is not False:
            try:
                curl.setopt(pycurl.PIPEWAIT, 1)
            except pycurl.error as e:
                assert e.args[0] is pycurl.E_NOT_BUILT_IN
                self.pipewait_supprted = False
            except AttributeError:
                self.pipewait_supprted = False

        # CURLOPT_TCP_FASTOPEN - enable TCP Fast Open
        # https://curl.haxx.se/libcurl/c/CURLOPT_TCP_FASTOPEN.html
        # TCP Fast Open (RFC7413) is a mechanism that allows data to be carried in the SYN and SYN-ACK packets and consumed by the receiving end during the initial connection handshake, saving up to one full round-trip time (RTT).
        if self.tcp_fastopen_supported is not False:
            try:
                # This option is currently only supported on Linux and OS X El Capitan.
                # Not supported on windows.
                curl.setopt(pycurl.TCP_FASTOPEN, 1)
            except pycurl.error as e:
                assert e.args[0] is pycurl.E_NOT_BUILT_IN
                self.tcp_fastopen_supported = False
            except AttributeError:
                self.tcp_fastopen_supported = False

        # last_opt = None
        # for opt in self.extra_opts:
        #   if opt == last_opt:
        #       continue
        #   else:
        #       last_opt = opt

        #   opt_name, opt_value = opt
        #   curl.setopt(opt_name, opt_value)

        for opt_name, opt_value in self.extra_opts:
            if DEBUG:
                print('setopt', (opt_name, opt_value))
            curl.setopt(opt_name, opt_value)

        if DEBUG:
            print('return curl', curl.request.id)
        return curl

    def __enter__(self):
        return self

    def __exit__(self, *args):
        # self.close()
        # TODO
        return

    def __getstate__(self):
        state = {attr: getattr(self, attr, None) for attr in self.__attrs__}
        return state

    def __setstate__(self, state):
        for attr, value in state.items():
            setattr(self, attr, value)


def session(*args, **kwargs):
    """
    Returns a :class:`Session`.
    .. deprecated:: Requests 1.0.0
        This method has been deprecated since Requests version 1.0.0 and is only kept for
        backwards compatibility.
    :rtype: Session
    """
    return Session(**locals())
