DEBUG = 0
VERBOSE = 0

__title__ = 'curl-requests'
# __version__ = '1.0.0'
__version__ = '0.0.1'
__build__ = 0x000000
__author__ = 'ritu'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2019 ritu'

# https://github.com/kennethreitz/requests/blob/master/requests/__init__.py
# from . import utils
# from . import packages
# from .models import Request, Response, PreparedRequest
# from .api import request, get, head, post, patch, put, delete, options
# from .curl_session import CurlSession
from .curl_session import Session, session
# from .status_codes import codes
# import exceptions
from .exceptions import (
    RequestException, TooManyRedirects, HTTPError, ConnectionError, Timeout, ConnectTimeout, ReadTimeout
)
