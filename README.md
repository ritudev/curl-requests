# CurlRequests: HTTP/2 Curl Requests for Humans

CurlRequests uses [PycURL](https://github.com/pycurl/pycurl) (a [high performance](https://stackoverflow.com/a/32899936) interface to the C-based libcurl) while remaining user friendly and easy to use like [Requests](https://github.com/psf/requests).

CurlRequests implements a number of methods from Requests and in many scenarios can be considered a high performance drop-in replacement for Requests.

The main motivation behind the making of CurlRequests was the need for multiplexing HTTP/2 requests which is what curl is great for, but what is difficult to implement in PycURL.

# Examples
## HTTP/2 GET
```python
>>> import curl_requests
>>> curl_session = curl_requests.Session()
>>> r = curl_session.get('https://http2.golang.org/reqinfo')
>>> r.status_code
200
>>> r.headers['content-type']
'text/plain'
>>> r.content[:30]
b'Method: GET\nProtocol: HTTP/2.0'
>>> r.text[:30]
'Method: GET\nProtocol: HTTP/2.0'
```

## HTTP/2 POST json
```python
>>> import curl_requests
>>> curl_session = curl_requests.Session()
>>> r = curl_session.post('https://nghttp2.org/httpbin/post', json={'test_key':'test_value'})
>>> r.json()['data']
'{"test_key": "test_value"}'
>>> r.json()['headers']['User-Agent']
'PycURL/7.43.0.3 libcurl/7.65.2-DEV OpenSSL/1.1.1b zlib/1.2.11 brotli/1.0.7 nghttp2/1.36.0'
>>> r.getinfo.version
'HTTP/2.0'
>>> r.getinfo.total_time
1.056283
```

## HTTP/2 multiplexing example #1: Sending 2 asynchronous requests and sharing session cookies
You can use CurlRequests in the same fashion as in [requests-futures](https://github.com/ross/requests-futures) by setting `block=True`.
Unlike HTTP/1.1 that spawns a separate TCP connection per simultaneous request, the HTTP/2 protocol only needs a a single connection that then gets multiplexed for each simultaneous request.
```python
import curl_requests

curl_session = curl_requests.Session(pool_connections=10)
curl_session.cookies.set(name='aaaa', value='bbbb', domain='nghttp2.org', path='/')

# pass the block=False argument to not wait for the request to complete
request_1 = curl_session.get('https://nghttp2.org/httpbin/cookies/set?foo1=123&bar1=456', block=False)
request_2 = curl_session.get('https://nghttp2.org/httpbin/cookies/set?foo2=abc&bar2=xyz', block=False)
print ('request_1', request_1)  # request_1 <RunningRequest [GET]>
print ('request_2', request_2)  # request_2 <RunningRequest [GET]>

# wait for every request to complete, if it hasn't already
response_1 = request_1.result()
response_2 = request_2.result()

print ('response_1', response_1)  # response_1 <Response [200]>
print ('response_2', response_2)  # response_2 <Response [200]>
for cookie in curl_session.cookies:
    print (cookie)
    # <Cookie aaaa=bbbb for nghttp2.org/>
    # <Cookie foo1=123 for nghttp2.org/>
    # <Cookie bar1=456 for nghttp2.org/>
    # <Cookie foo2=abc for nghttp2.org/>
    # <Cookie bar2=xyz for nghttp2.org/>
```

## HTTP/2 multiplexing example #2: CURLOPT_PIPEWAIT 
Optionally, let's set the [CURLOPT_PIPEWAIT](https://curl.haxx.se/libcurl/c/CURLOPT_PIPEWAIT.html) flag to 1 or True to instruct curl to wait for pipelining/multiplexing.

While libcurl sets up a connection to a HTTP server there is a period during which it doesn't know if it can pipeline or do multiplexing and if you add new transfers in that period, libcurl will default to start new connections for those transfers. With the new option CURLOPT_PIPEWAIT (added in 7.43.0), you can ask that a transfer should rather wait and see in case there's a connection for the same host in progress that might end up being possible to multiplex on. It favours keeping the number of connections low at the cost of slightly longer time to first byte transferred.

The first request sets up the HTTP/2 TCP connection and because of pipewait=True, the subsequent requests wait for its TCP connection to establish instead of potentially opening multiple TCP connections. pipewait=true is basically the same as sending a single synchronous request to establish the connection, and then following it up with multiple asynchronous requests that would reuse that connection.

```python
import curl_requests
import time

curl_session = curl_requests.Session(pool_connections=10)
curl_session.pipewait = True

# enable verbose output in curl for testing
curl_session.verbose = True

# simultaneously send 10 HTTP/2 requests, each reusing the same TCP connection
time_start = time.time()
reqs = []
for request_idx in range(10):
    url = 'https://nghttp2.org/httpbin/status/200?test='+str(request_idx)
    request = curl_session.get(url, block=False)
    reqs.append((request_idx, request))

# * Trying 139.162.123.134...
# * TCP_NODELAY set
# * Connected to nghttp2.org (139.162.123.134) port 443 (#0)
# * ALPN, offering h2
# * ALPN, offering http/1.1
# * ALPN, server accepted to use h2
# * Using HTTP2, server supports multi-use

# curl's verbose output shows that we are multiplexing the HTTP/2 TCP connection:
# request_idx #0:
# * Found bundle for host nghttp2.org: 0x27f5570 [can multiplex]
# * Re-using existing connection! (#0) with host nghttp2.org
# ...
# request_idx #9:
# * Found bundle for host nghttp2.org: 0x27f5570 [can multiplex]
# * Multiplexed connection found!
# * Found connection 0, with requests in the pipe (8)

# We successfully multiplexed 10 simultaneous requests on a single TCP connection.
# If this was HTTP/1.1 then opening 10 TCP connections would be necessary.

# wait for every request to complete, if it hasn't already
for request_idx, running_request in reqs:
    r = running_request.result()
    print(('request #{} completed in {:.3f} seconds: {}'.format(request_idx, r.getinfo.total_time, r)))
time_end = time.time()
print(('completed all {} requests in {:.3f} seconds'.format(len(reqs), time_end-time_start)))
# request #0 completed in 0.545 seconds: <Response [200]>
# request #1 completed in 0.543 seconds: <Response [200]>
# request #2 completed in 0.543 seconds: <Response [200]>
# request #3 completed in 0.543 seconds: <Response [200]>
# request #4 completed in 0.543 seconds: <Response [200]>
# request #5 completed in 0.543 seconds: <Response [200]>
# request #6 completed in 0.362 seconds: <Response [200]>
# request #7 completed in 0.362 seconds: <Response [200]>
# request #8 completed in 0.526 seconds: <Response [200]>
# request #9 completed in 0.527 seconds: <Response [200]>
# completed all 10 requests in 0.711 seconds

```

## HTTP/2 multiplexing bonus example: Ventilator
If for some reason you would like to use CurlRequests as, for example, a message broker (like in the ZeroMQ ventilator examples [[1]](http://zguide.zeromq.org/py:taskvent), [[2]](http://mdup.fr/blog/easy-cluster-parallelization-with-zeromq)) which send jobs to worker(s) and you do not wish to ever wait with `.result()` for requests to finish, then you can do so by passing the `ventilator=True` flag! Setting CurlRequests to `ventilator=True` will automatically set it to `block=False`, and the only real difference between using `block=False` with ventilator or not is that the `ventilator=True` argument will ignore all responses and will garbage collect them instead, making it impossible to read the response with `.result()` but preventing memory leaks and saving you some CPU cycles.

```python
curl_session = curl_requests.Session(pool_connections=10)
curl_session.pipewait = True

# simultaneously send 20 HTTP/2 requests, never reading any responses.
for request_idx in range(20):
    url = 'https://nghttp2.org/httpbin/status/200?test='+str(request_idx)
    request = curl_session.get(url, block=False, ventilator=True)
    print (request_idx, request)
    # request #0  sent at 2019-07-28 20:19:16.945710: <RunningRequest [GET]>
    # request #1  sent at 2019-07-28 20:19:16.946012: <RunningRequest [GET]>
    # request #2  sent at 2019-07-28 20:19:16.946245: <RunningRequest [GET]>
    # request #3  sent at 2019-07-28 20:19:16.946517: <RunningRequest [GET]>
    # request #4  sent at 2019-07-28 20:19:16.946758: <RunningRequest [GET]>
    # request #5  sent at 2019-07-28 20:19:16.946955: <RunningRequest [GET]>
    # request #6  sent at 2019-07-28 20:19:16.947246: <RunningRequest [GET]>
    # request #7  sent at 2019-07-28 20:19:16.947448: <RunningRequest [GET]>
    # request #8  sent at 2019-07-28 20:19:16.947633: <RunningRequest [GET]>
    # request #9  sent at 2019-07-28 20:19:16.947824: <RunningRequest [GET]>
    # request #10 sent at 2019-07-28 20:19:18.013326: <RunningRequest [GET]>
    # request #11 sent at 2019-07-28 20:19:18.014802: <RunningRequest [GET]>
    # request #12 sent at 2019-07-28 20:19:18.016342: <RunningRequest [GET]>
    # request #13 sent at 2019-07-28 20:19:18.017838: <RunningRequest [GET]>
    # request #14 sent at 2019-07-28 20:19:18.019503: <RunningRequest [GET]>
    # request #15 sent at 2019-07-28 20:19:18.020848: <RunningRequest [GET]>
    # request #16 sent at 2019-07-28 20:19:18.021199: <RunningRequest [GET]>
    # request #17 sent at 2019-07-28 20:19:18.022570: <RunningRequest [GET]>
    # request #18 sent at 2019-07-28 20:19:18.194424: <RunningRequest [GET]>
    # request #19 sent at 2019-07-28 20:19:18.195757: <RunningRequest [GET]>
```
Notice how there was a pause after the request #9. This is because we passed `pool_connections=10` to our Curl Session, which instructed CurlRequests to create 10 curl handles, allowing us to send a maximum of 10 asynchronous requests. `pool_connections=10` is the default (similar to Requests), but you can increase it to something like 100 to create 100 curl handles to reuse in the session.

## Exceptions
CurlRequests translates PycURL exceptions to Requests exceptions:
```python
>>> import requests
>>> import curl_requests
>>> curl_session = curl_requests.Session()
>>> try:
...     r = curl_session.get('http://httpstat.us/302?sleep=5000', allow_redirects=False, verify=False, timeout=0.001)
... except requests.exceptions.Timeout as e:
...     print (e)
...
[#28]: Connection timed out after 82 milliseconds
>>> try:
...     r = curl_session.get('http://httpstat.us/302?sleep=5000', allow_redirects=False, verify=False, timeout=2.5)
... except requests.exceptions.Timeout as e:
...     print (e)
...
[#28]: Operation timed out after 2670 milliseconds with 0 bytes received
```
For the first request with CONNECT timeout, requests.exceptions.ConnectTimeout would also work.

For the second request with a read timeout, requests.exceptions.ReadTimeout would also work.

the `[#28]` error code is a reference to libcurl: https://curl.haxx.se/libcurl/c/libcurl-errors.html

## Options (they work like in the Requests library):
*  `proxies=None` - pass a dict like in Requests, or just a proxy:port string.
*  `verify=None` - verification handled by certifi, set to False to disable.
*  `allow_redirects=True`
*  `max_redirects=30`
*  `max_retries=10` - not implemented.
*  `pool_connections=10`  - number of curl handles to create and reuse i.e. what's the max simultaneous requests that Curl Session can perform.

## Extra options:
*  `use_cookies=True` - set to False to disable cookies alltogether.
*  `cookie_whitelist=None` - if you wish to whitelist only some cookies and discard the rest, then pass a list of cookie names e.g. `['__cfduid', 'cf_clearance']`
*  `pipewait=False` - [CURLOPT_PIPEWAIT](https://curl.haxx.se/libcurl/c/CURLOPT_PIPEWAIT.html)
*  `verbose=None` - set to True to enable the curl's verbose log. very useful for learning and debugging.
*  `block=True` - set to False to send a non-blocking request. then wait for the response with `.result()`. 
*  `ventilator=False` - nobody will ever use it.

## Gotchas:
*  CurlRequests defaults to HTTP/2 for servers that support it. http://httpbin.org/ only supports HTTP/1.1 which is why https://nghttp2.org/httpbin/ is used in the HTTP/2 examples.
*  If you test with a debugging proxy like Fiddler or mitmproxy then the requests will always show up as HTTP/1.1 because those proxies don't support HTTP/2.
*  CurlRequests uses [CURLOPT_TCP_FASTOPEN](https://curl.haxx.se/libcurl/c/CURLOPT_TCP_FASTOPEN.html) if it's available and enabled on your distro.
*  CurlRequests aims to be a performant drop-in replacement for Requests, however, some/many of its methods aren't implemented.
*  CurlRequests is thread-safe.
*  CurlRequests will use 0 threads if you use it synchronously, or 1 thread in total if you use it asynchronously with `block=False`. C-based libcurl does all the concurrency for us.