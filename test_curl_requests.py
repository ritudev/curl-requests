"""Tests for Requests."""

from unittest import TestCase, main, skipIf
import curl_requests


class RequestsTestCase(TestCase):
    def test_curl_session(self):
        # basic futures get
        curl_session = curl_requests.Session()
        resp = curl_session.get('https://httpbin.org/get')
        self.assertEqual(200, resp.status_code)

    def test_nonblocking(self):
        curl_session = curl_requests.Session(pool_connections=10)
        # pass the block=False argument to not wait for the request to complete
        request_1 = curl_session.get('https://httpbin.org/get', block=False)
        request_2 = curl_session.get('https://httpbin.org/get', block=False)

        # wait for everrespy request to complete, if it hasn't already
        response_1 = request_1.result()
        response_2 = request_2.result()
        self.assertEqual(200, response_1.status_code)
        self.assertEqual(200, response_2.status_code)

    def test_set_cookie(self):
        curl_session = curl_requests.Session()
        curl_session.cookies.set(name='aaaa', value='bbbb', domain='httpbin.org', path='/')
        self.assertTrue('aaaa' in curl_session.cookies)


if __name__ == '__main__':
    main()
